#!/bin/sh

set -e

import() {
    workload="${1?}"
    name="`basename "${workload%.json*}"`"

    if [ ! -e "$name/cwa/config" ]; then
        create_project "$name" "$workload"
    fi

    if [ ! -e "$name/cwa/traces/trace.cwtd.gz" ]; then
        echo "Importing $name"

        create_project "$name" "$workload"
        
        # Analyze, Compress, Cleanup
        pushd "$name"
        cwa import
        gzip cwa/traces/*
        popd

        rm -rf "$name/raw"
    fi
}

analyze() {
    workload="${1?}"
    name="`basename "${workload%.json*}"`"

    if [ ! -e "$name/cwa/config" ]; then
        create_project "$name"
    fi

    if [ -e "$name/cwa/traces/trace.cwtd.gz" ]; then
        echo "Analyzing $name"

        # Analyze, Compress, Cleanup
        pushd "$name"
        cwa analyze
        popd
    fi
}

create_project() {
    name="${1?}"
    workload="${2?}"

    for d in "$name" "$name/raw" "$name/cwa" "$name/cwa/traces" "$name/cwa/data" "$name/cwa/plots"; do
        if [ ! -d "$d" ]; then
            mkdir -p "$d"
        fi
    done

    if [ ! -e "$name/cwa/config" ]; then
cat <<EOD >"$name/cwa/config"
[trace]
title = $name

[import]
input           = %(path)s/../$workload
module          = cwa.import.rumen

[analyze]
job_metrics     = RunTime
job_breakdowns  = ExecutableID
task_metrics    = RunTime
task_breakdowns = ExecutableID MR_task_type

[plot]
job_metrics     = RunTime
job_breakdowns  = ExecutableID
task_metrics    = RunTime
task_breakdowns = ExecutableID MR_task_type
EOD
    fi
}


# Finally... Run!
#for workload in ../workloads/workload*.json*; do
for workload in ../workloads/workload-06h*.json*; do
    import "$workload"
#done

#for workload in ../workloads/workload-01h*0s.json*; do
    analyze "$workload"
done
