START TIME:          Fri Jan 20 16:39:36 CET 2012
WORKLOAD GENERATED:  
END TIME:            Fri Jan 20 18:37:45 CET 2012

MUMAK TIME:
6850.60user 1269.63system 1:58:08elapsed 114%CPU (0avgtext+0avgdata 20913312maxresident)k
70256inputs+6736696outputs (50major+82767817minor)pagefaults 0swaps

WORKLOAD_MODEL:      /home/deruiter/work/mumak-on-sge/etc/model.json
WORKLOAD_SEED:       None
WORKLOAD_LOAD:       100
WORKLOAD_START:      600
WORKLOAD_RUNTIME:    1
TOPOLOGY_FILE:       /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz
MUMAK_WORKLOAD_FILE: /var/scratch/deruiter/workloads/workload-06h-06180l-0s.json.gz
MUMAK_TOPOLOGY_FILE: /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz

Mumak Workload MD5:
d2734a551d2de59cc31c2f87d8d41be5  /var/scratch/deruiter/workloads/workload-06h-06180l-0s.json.gz

Workload Model:
{
  "application_0": {
    "interarrivaltime": [
      "weibull",
      50,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      5,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      200,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.85,
      0.05
    ],
    "runtime": [
      "weibull",
      60,
      0.5
    ]
  },
  "application_1": {
    "interarrivaltime": [
      "weibull",
      20,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      1,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      20,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.9,
      0.05
    ],
    "runtime": [
      "weibull",
      6,
      0.5
    ]
  }
}
