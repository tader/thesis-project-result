START TIME:          Sat Jan 21 02:11:22 CET 2012
WORKLOAD GENERATED:  
END TIME:            Sat Jan 21 07:02:40 CET 2012

MUMAK TIME:
15604.31user 4206.05system 4:51:18elapsed 113%CPU (0avgtext+0avgdata 26125680maxresident)k
58536inputs+10010312outputs (0major+116593246minor)pagefaults 0swaps

WORKLOAD_MODEL:      /home/deruiter/work/mumak-on-sge/etc/model.json
WORKLOAD_SEED:       None
WORKLOAD_LOAD:       100
WORKLOAD_START:      600
WORKLOAD_RUNTIME:    1
TOPOLOGY_FILE:       /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz
MUMAK_WORKLOAD_FILE: /var/scratch/deruiter/workloads/workload-06h-09888l-1s.json.gz
MUMAK_TOPOLOGY_FILE: /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz

Mumak Workload MD5:
7bc5f87356b3796630713504e7955359  /var/scratch/deruiter/workloads/workload-06h-09888l-1s.json.gz

Workload Model:
{
  "application_0": {
    "interarrivaltime": [
      "weibull",
      50,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      5,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      200,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.85,
      0.05
    ],
    "runtime": [
      "weibull",
      60,
      0.5
    ]
  },
  "application_1": {
    "interarrivaltime": [
      "weibull",
      20,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      1,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      20,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.9,
      0.05
    ],
    "runtime": [
      "weibull",
      6,
      0.5
    ]
  }
}
