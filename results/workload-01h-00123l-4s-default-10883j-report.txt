START TIME:          Wed Jan 18 17:59:58 CET 2012
WORKLOAD GENERATED:  
END TIME:            Wed Jan 18 18:00:52 CET 2012

MUMAK TIME:
62.18user 3.08system 0:53.74elapsed 121%CPU (0avgtext+0avgdata 2495152maxresident)k
54080inputs+24704outputs (62major+398752minor)pagefaults 0swaps

WORKLOAD_MODEL:      /home/deruiter/work/mumak-on-sge/etc/model.json
WORKLOAD_SEED:       None
WORKLOAD_LOAD:       100
WORKLOAD_START:      600
WORKLOAD_RUNTIME:    1
TOPOLOGY_FILE:       /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz
MUMAK_WORKLOAD_FILE: /var/scratch/deruiter/workloads/workload-01h-00123l-4s.json.gz
MUMAK_TOPOLOGY_FILE: /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz

Mumak Workload MD5:
903d29ea0cd68c5b0af025e921014551  /var/scratch/deruiter/workloads/workload-01h-00123l-4s.json.gz

Workload Model:
{
  "application_0": {
    "interarrivaltime": [
      "weibull",
      50,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      5,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      200,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.85,
      0.05
    ],
    "runtime": [
      "weibull",
      60,
      0.5
    ]
  },
  "application_1": {
    "interarrivaltime": [
      "weibull",
      20,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      1,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      20,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.9,
      0.05
    ],
    "runtime": [
      "weibull",
      6,
      0.5
    ]
  }
}
