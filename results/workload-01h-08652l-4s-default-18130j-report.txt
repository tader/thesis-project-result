START TIME:          Wed Jan 18 23:11:50 CET 2012
WORKLOAD GENERATED:  
END TIME:            Thu Jan 19 00:05:36 CET 2012

MUMAK TIME:
3190.67user 163.13system 53:46.45elapsed 103%CPU (0avgtext+0avgdata 16530048maxresident)k
52264inputs+1363400outputs (62major+10881909minor)pagefaults 0swaps

WORKLOAD_MODEL:      /home/deruiter/work/mumak-on-sge/etc/model.json
WORKLOAD_SEED:       None
WORKLOAD_LOAD:       100
WORKLOAD_START:      600
WORKLOAD_RUNTIME:    1
TOPOLOGY_FILE:       /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz
MUMAK_WORKLOAD_FILE: /var/scratch/deruiter/workloads/workload-01h-08652l-4s.json.gz
MUMAK_TOPOLOGY_FILE: /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz

Mumak Workload MD5:
fef817ca07870843353fb1c57314bc59  /var/scratch/deruiter/workloads/workload-01h-08652l-4s.json.gz

Workload Model:
{
  "application_0": {
    "interarrivaltime": [
      "weibull",
      50,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      5,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      200,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.85,
      0.05
    ],
    "runtime": [
      "weibull",
      60,
      0.5
    ]
  },
  "application_1": {
    "interarrivaltime": [
      "weibull",
      20,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      1,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      20,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.9,
      0.05
    ],
    "runtime": [
      "weibull",
      6,
      0.5
    ]
  }
}
