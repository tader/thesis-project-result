START TIME:          Thu Jan 19 12:15:16 CET 2012
WORKLOAD GENERATED:  
END TIME:            Thu Jan 19 12:18:51 CET 2012

MUMAK TIME:
221.99user 8.92system 3:33.58elapsed 108%CPU (0avgtext+0avgdata 6049728maxresident)k
54812inputs+93744outputs (70major+763840minor)pagefaults 0swaps

WORKLOAD_MODEL:      /home/deruiter/work/mumak-on-sge/etc/model.json
WORKLOAD_SEED:       None
WORKLOAD_LOAD:       100
WORKLOAD_START:      600
WORKLOAD_RUNTIME:    1
TOPOLOGY_FILE:       /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz
MUMAK_WORKLOAD_FILE: /var/scratch/deruiter/workloads/workload-06h-00123l-0s.json.gz
MUMAK_TOPOLOGY_FILE: /home/deruiter/work/mumak-on-sge/etc/19-jobs.topology.json.gz

Mumak Workload MD5:
06a450f78fa00273ce668a375a4b1c05  /var/scratch/deruiter/workloads/workload-06h-00123l-0s.json.gz

Workload Model:
{
  "application_0": {
    "interarrivaltime": [
      "weibull",
      50,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      5,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      200,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.85,
      0.05
    ],
    "runtime": [
      "weibull",
      60,
      0.5
    ]
  },
  "application_1": {
    "interarrivaltime": [
      "weibull",
      20,
      0.5
    ],
    "attempt_interarrivaltime": [
      "weibull",
      1,
      0.5
    ],
    "waittime": [
      "weibull",
      2,
      0.5
    ],
    "numtasks": [
      "weibull",
      20,
      0.5
    ],
    "mapreduceratio": [
      "normal",
      0.9,
      0.05
    ],
    "runtime": [
      "weibull",
      6,
      0.5
    ]
  }
}
