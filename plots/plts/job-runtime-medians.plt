set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "./output/job-runtime-medians.eps"

set key below

set lmargin at screen 0.13
set rmargin at screen 0.96

set pointsize 2.0

set style line 1 linewidth 2.0 pointtype 9
set style line 2 linewidth 2.0 pointtype 7

set ylabel "RunTime (seconds)"
set xlabel "Average Load (%)"
set xrange [0:100]
set xtics 10

plot "./data/job-runtime-medians-default.dat" using 1:4 with linespoints title "default policy" ls 1, \
     "./data/job-runtime-medians-fair.dat" using 1:4 with linespoints title "fair policy" ls 2
