set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "./output/task-runtime-medians-m.eps"

set key below

set lmargin at screen 0.13
set rmargin at screen 0.96

set pointsize 2.0

set style line 1 linewidth 2.0 pointtype 9
set style line 2 linewidth 2.0 pointtype 7

set ylabel "RunTime_per_MR_task_type_[0] (seconds)"
set xlabel "Average Load (%)"
set xrange [0:100]
set yrange [0:]
set xtics 10

plot "./data/task-runtime-medians-m-default.dat" using 1:4 with linespoints title "default policy" ls 1, \
     "./data/task-runtime-medians-m-fair.dat" using 1:4 with linespoints title "fair policy" ls 2, \
     "./data/task-runtime-medians-m-workload.dat" using 1:4 with linespoints title "generated workload" ls 3
