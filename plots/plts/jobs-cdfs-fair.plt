#set terminal postscript eps enhanced color size 14.8cm,7.4cm #A5
set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "./output/jobs-cdfs-fair.eps"

set style line 1 linewidth 5.0
set style line 2 linewidth 2.0
set style line 3 linewidth 2.0
set style line 4 linewidth 2.0
set style line 5 linewidth 2.0

set lmargin at screen 0.13
set rmargin at screen 0.96

set key bottom right

set xrange [0:50000]

set yrange [0:1.1]
set ylabel "Probability"
set xlabel "Run Time (seconds)"
plot "../analyses/workload-06h-11124l-3s-fair/cwa/data/cumulative_job_statistics_RunTime_pdf_cdf.dat" using 1:3 with lines ls 1 title "90% Average Load", \
     "../analyses/workload-06h-09888l-3s-fair/cwa/data/cumulative_job_statistics_RunTime_pdf_cdf.dat" using 1:3 with lines ls 2 title "80% Average Load", \
     "../analyses/workload-06h-07416l-3s-fair/cwa/data/cumulative_job_statistics_RunTime_pdf_cdf.dat" using 1:3 with lines ls 3 title "60% Average Load", \
     "../analyses/workload-06h-04944l-3s-fair/cwa/data/cumulative_job_statistics_RunTime_pdf_cdf.dat" using 1:3 with lines ls 4 title "40% Average Load", \
     "../analyses/workload-06h-03708l-3s-fair/cwa/data/cumulative_job_statistics_RunTime_pdf_cdf.dat" using 1:3 with lines ls 5 title "30% Average Load"
