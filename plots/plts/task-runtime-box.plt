set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "./output/task-runtime-box.eps"

set key below

set lmargin at screen 0.13
set rmargin at screen 0.96

set boxwidth 1.25
set log y

set style line 1 linewidth 2.0 pointtype 9
set style line 2 linewidth 2.0 pointtype 7

set ylabel "RunTime (seconds)"
set xlabel "Average Load (%)"
set xrange [-5:100]
set yrange [1:100000]
set xtics 10

plot "./data/task-runtime-box-default.dat" using ($1-2):3:2:6:5 with candlesticks title   "default policy" ls 1 whiskerbars, \
     "./data/task-runtime-box-default.dat" using ($1-2):4:4:4:4 with candlesticks notitle            ls 1, \
     "./data/task-runtime-box-fair.dat" using   ($1):3:2:6:5 with candlesticks title   "fair policy" ls 2 whiskerbars, \
     "./data/task-runtime-box-fair.dat" using   ($1):4:4:4:4 with candlesticks notitle            ls 2, \
     "./data/task-runtime-box-workload.dat" using ($1+2):3:2:6:5 with candlesticks title   "generated workload" ls 3 whiskerbars, \
     "./data/task-runtime-box-workload.dat" using ($1+2):4:4:4:4 with candlesticks notitle            ls 3
