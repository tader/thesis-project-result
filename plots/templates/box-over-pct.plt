set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "%%OUTPUT%%.eps"

set key below

set lmargin at screen 0.13
set rmargin at screen 0.96

set boxwidth 1.25
set log y

set style line 1 linewidth 2.0 pointtype 9
set style line 2 linewidth 2.0 pointtype 7

set ylabel "%%WHAT%%"
set xlabel "Average Load (%)"
set xrange [-5:100]
set yrange [1:100000]
set xtics 10

plot "%%DATA1%%" using ($1-1):3:2:6:5 with candlesticks title   "%%KEY1%%" ls 1 whiskerbars, \
     "%%DATA1%%" using ($1-1):4:4:4:4 with candlesticks notitle            ls 1, \
     "%%DATA2%%" using ($1+1):3:2:6:5 with candlesticks title   "%%KEY2%%" ls 2 whiskerbars, \
     "%%DATA2%%" using ($1+1):4:4:4:4 with candlesticks notitle            ls 2
