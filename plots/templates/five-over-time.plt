#set terminal postscript eps enhanced size 21.0cm,29.7cm #A4
#set terminal postscript eps enhanced color size 14.8cm,21.0cm #A5
set terminal postscript eps enhanced color size 14.8cm,12.0cm #A5
#set terminal postscript eps enhanced size 10.5cm,14.8cm #A6
set output "%%OUTPUT%%.eps"

set style line 1 linewidth 2.0
set style line 2 linewidth 2.0

set lmargin at screen 0.13
set rmargin at screen 0.96
set bmargin 0
set tmargin 0

#set grid
set yrange [0:%%YRANGE%%]
set xrange [0:%%XRANGE%%]
set mxtics 6
set nokey
set format x ""

set ytics %%YTICS%%

set multiplot

set size 1.0,0.18

set origin 0.0,0.82
set ylabel "90% Load\n\n%%WHAT%% Count"
plot "%%DATA1_90%%" using ($1/3600):2 with steps ls 1 title "%%KEY1%%", \
     "%%DATA2_90%%" using ($1/3600):2 with steps ls 2 title "%%KEY2%%"


set origin 0.0,0.64
set ylabel "80% Load\n\n%%WHAT%% Count"
plot "%%DATA1_80%%" using ($1/3600):2 with steps ls 1 title "%%KEY1%%", \
     "%%DATA2_80%%" using ($1/3600):2 with steps ls 2 title "%%KEY2%%"

set origin 0.0,0.46
set ylabel "60% Load\n\n%%WHAT%% Count"
plot "%%DATA1_60%%" using ($1/3600):2 with steps ls 1 title "%%KEY1%%", \
     "%%DATA2_60%%" using ($1/3600):2 with steps ls 2 title "%%KEY2%%"

set origin 0.0,0.28
set ylabel "40% Load\n\n%%WHAT%% Count"
plot "%%DATA1_40%%" using ($1/3600):2 with steps ls 1 title "%%KEY1%%", \
     "%%DATA2_40%%" using ($1/3600):2 with steps ls 2 title "%%KEY2%%"

set key below
set format x
set xlabel "Time since workload start (hours)"
set bmargin

set size 1.0,0.28
set origin 0.0,0.0
set ylabel "30% Load\n\n%%WHAT%% Count"
plot "%%DATA1_30%%" using ($1/3600):2 with steps ls 1 title "%%KEY1%%", \
     "%%DATA2_30%%" using ($1/3600):2 with steps ls 2 title "%%KEY2%%"


unset multiplot
