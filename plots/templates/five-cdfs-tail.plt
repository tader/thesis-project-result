#set terminal postscript eps enhanced color size 14.8cm,7.4cm #A5
set terminal postscript eps enhanced color size 14.8cm,4.2cm #A5
set output "%%OUTPUT%%.eps"

set style line 1 linewidth 5.0
set style line 2 linewidth 2.0
set style line 3 linewidth 2.0
set style line 4 linewidth 2.0
set style line 5 linewidth 2.0



%%OPTIONS%%

set multiplot
set origin 0.0,0.0

set lmargin at screen 0.13
set rmargin at screen 0.7
set nokey
set yrange [0:1.1]
set ylabel "Probability"
set xlabel "Run Time (seconds)"
plot "%%DATA1%%" using 1:3 with lines ls 1 title "90% Average Load", \
     "%%DATA2%%" using 1:3 with lines ls 2 title "80% Average Load", \
     "%%DATA3%%" using 1:3 with lines ls 3 title "60% Average Load", \
     "%%DATA4%%" using 1:3 with lines ls 4 title "40% Average Load", \
     "%%DATA5%%" using 1:3 with lines ls 5 title "30% Average Load"


set key bottom right

set origin 0.0,0.0
set lmargin at screen 0.7
set rmargin at screen 0.96
set log x
unset ylabel
set format y ""
set xlabel " "
set xrange [5000:100000]
plot "%%DATA1%%" using 1:3 with lines ls 1 title "90% Average Load", \
     "%%DATA2%%" using 1:3 with lines ls 2 title "80% Average Load", \
     "%%DATA3%%" using 1:3 with lines ls 3 title "60% Average Load", \
     "%%DATA4%%" using 1:3 with lines ls 4 title "40% Average Load", \
     "%%DATA5%%" using 1:3 with lines ls 5 title "30% Average Load"

unset multiplot
